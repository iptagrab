/*
 * $Id: iptabgrab.c,v 1.4 2007-12-15 20:23:05 kt Exp $
 *
 * Copyright (c) 2005-2006 Karel Tuma, leet.cz.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * Binds to a TCP port and reports given ipt_{account|ACCOUNT} table 
 * contents every second. Daemon accepts arbitrary number of clients,
 * no buffering is done -> client not capable of receiving the buffer
 * within TICK period will be disconnected.
 *
 * To compile with the libipt_ACCOUNT interface:
 *	gcc -Wall -O2 iptagrab.c -lipt_ACCOUNT -I/usr/local/include -I. -o iptagrab
 *
 * To compile with the libipt_account interface:
 *      gcc -Wall -O2 -DPROCIPTACCOUNT iptagrab.c -o iptagrab
 *
 * Output format:
 * <timestamp> <ip> <src pkts> <src bytes> <dst pkts> <dst bytes>
 *
 * Added sqlite accounting feature, table schema:
 * CREATE TABLE iptraf (pkt_up integer, b_up integer,
 * 	pkt_dn integer, b_dn integer, time integer,
 * 	ip string, <your fields>, unique(ip,time));
 *
 * First 6 fields are hardcoded to their positions,
 * you can use custom fields after that.
 *
 * TODO: be nicer beyond simple asserts()
 */


/* settings */
#define PORT 5654
#define BIND "127.0.0.1"
#define TICK 10
#define LINELEN 512
//#define SQLITE 300	/* store to sqlite in 5minute rows, comment out */

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <signal.h>
#include <string.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <stdarg.h>

#ifndef PROCIPTACCOUNT
#include <ipt_ACCOUNT_cl.h>
#endif


time_t ts;

#ifdef SQLITE
#include <sqlite3.h>
sqlite3 *dbh;
sqlite3_stmt *stup, *stins;

#define BINDVAL(st) { \
	sqlite3_bind_int(st,1,ps);\
	sqlite3_bind_int(st,2,bs);\
	sqlite3_bind_int(st,3,pd);\
	sqlite3_bind_int(st,4,bd);\
	sqlite3_bind_int(st,5,ts/SQLITE);\
	sqlite3_bind_text(st,6,ip,strlen(ip),NULL);}

void	db_record(char *ip, uint32_t ps, uint32_t bs, uint32_t pd, uint32_t bd)
{
	sqlite3_reset(stup);
	BINDVAL(stup);
	sqlite3_step(stup);
	if ((sqlite3_changes(dbh))==0) {
		//printf("ip %s didnt exist, inserting\n", ip);
		sqlite3_reset(stins);
		BINDVAL(stins);
		sqlite3_step(stins);
	}
}

#else
#define db_record(...)
#endif

#ifndef PROCIPTACCOUNT
char	*getsnap(struct ipt_ACCOUNT_context *ctx, char *tab, char *buf, int *buflen)
{
	int need;
	struct ipt_acc_handle_ip *entry;
	char *p = buf;
	char ipbuf[64];

	/* neco chybi .. */
	if (ipt_ACCOUNT_read_entries(ctx, tab, 0)) {
		*buflen = 0;
		return buf;
	}

	need = LINELEN * ctx->handle.itemcount;
	if (*buflen < need) p = buf = realloc(buf, need);

        while ((entry = (void*) ipt_ACCOUNT_get_next_entry(ctx))) {
		sprintf(ipbuf, "%d.%d.%d.%d", entry->ip&0xff, entry->ip>>8&0xff, entry->ip>>16&0xff, entry->ip>>24);
		p += sprintf(p, "%u %s %u %u %u %u\n", ts, ipbuf,
			entry->src_packets, entry->src_bytes, entry->dst_packets, entry->dst_bytes);
		db_record(ipbuf,entry->src_packets,entry->src_bytes,entry->dst_packets,entry->dst_bytes);
	}
	*buflen = (p-buf);
	return buf;
}
#else
char	*getsnap(char *fn, char *buf, int *buflen)
{
	FILE *f;
	char ip[64];
	uint32_t bs, ps, bd, pd;
	int pos = 0;

	if (!(f = fopen(fn, "r+"))) {
		*buflen = 0;
		return buf;
	}
	/* kdyby nam to nejaky hracicka zmenil */
	fputs("show=src-or-dst\n", f); fflush(f);
	fputs("reset-on-read=yes\n", f); fflush(f);
	ts = (uint32_t) time(NULL);
	while (fscanf(f, "ip = %s "
		"bytes_src = %u %*u %*u %*u %*u "
		"packets_src = %u %*u %*u %*u %*u "
		"bytes_dst = %u %*u %*u %*u %*u "
		"packets_dst = %u %*u %*u %*u %*u time = %*s\n",
		ip, &bs, &ps, &bd, &pd) == 5) {
		if (pos+LINELEN > *buflen) {
			*buflen += 32*LINELEN;
			buf = realloc(buf, *buflen);
		}
		db_record(ip, ps, bs, pd, bd);
		pos += sprintf(buf + pos, "%u %s %u %u %u %u\n", (unsigned)ts, ip, ps, bs, pd, bd);
		//printf("%d\n", pos);
	}
	*buflen = pos;
	fclose(f);
	return buf;
}
#endif

static	int ERR(char *fmt, ...)
{
#if 1
	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	fprintf(stderr, "\n");
	/* n_n */
	exit(((unsigned long)fmt)&127);
#endif
	return 0;
}

int	main(int argc, char *argv[])
{
	int srvsock,fdmax,i,lport = PORT;
	char *snapbuf = NULL;
	int got; int snaplen = 0;

	static fd_set cliset, rset, wset;
	static int clibufpos[FD_SETSIZE+1];
	static struct sockaddr_in sa;
	static struct timeval tv;
#ifndef PROCIPTACCOUNT
	static struct ipt_ACCOUNT_context ctx;
	assert(!ipt_ACCOUNT_init(&ctx));
#else
	char fn[PATH_MAX+1];
#endif
	assert(((argc==2) || (argc==3)) || ERR("usage:\n%s <table name> [port]", argv[0]));
	if (argc==3)
		assert((lport = atoi(argv[2])) || ERR("port number expected")); /* not a port number */
#ifdef PROCIPTACCOUNT
	sprintf(fn, "/proc/net/xt_account/%s", argv[1]);
#endif


#ifdef SQLITE
	assert(sqlite3_open("iptagrab.db", &dbh) == SQLITE_OK);
	sqlite3_exec(dbh, "CREATE table iptraf (pkt_up INTEGER, b_up INTEGER, pkt_dn INTEGER, b_dn INTEGER, time INTEGER, ip TEXT, UNIQUE(ip,time))", NULL, NULL, NULL);
	assert(sqlite3_prepare(dbh, "UPDATE iptraf SET pkt_up=pkt_up+?,b_up=b_up+?,pkt_dn=pkt_dn+?,b_dn=b_dn+? WHERE time=? AND ip=?", -1, &stup, NULL)==SQLITE_OK);
	assert(sqlite3_prepare(dbh, "INSERT INTO iptraf values(?,?,?,?,?,?)\n", -1, &stins, NULL)==SQLITE_OK);
#endif


	assert(0 <= (srvsock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)));
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = inet_addr(BIND);
	sa.sin_port = htons(lport);
	assert(0 <= bind(srvsock,(void*) &sa,sizeof(sa)));
	assert(0 <= listen(srvsock, 5));
	fcntl(srvsock, F_SETFL, O_RDWR|O_NONBLOCK);

	tv.tv_sec = TICK;
	fdmax = srvsock+1;

	/* daemon */
	signal(SIGPIPE, SIG_IGN); close(0); close(1); close(2);
	if (fork()) return 0; setsid(); if (fork()) return 0;

	while (1) {
		/* zjisti deskriptory kde musime cekat na write .. */
		FD_ZERO(&wset);
		for (i = 0; i < fdmax; i++) if (FD_ISSET(i, &cliset) && clibufpos[i] < snaplen)
			FD_SET(i, &wset);

		/* pockame si na ne s kudlou za rohem .. */
		FD_SET(srvsock, &rset);
		got = select(fdmax, &rset, &wset, NULL, &tv);
		assert(got>=0);

		/* nova socka */
		if (FD_ISSET(srvsock, &rset)) {
			int newcli = accept(srvsock, NULL, NULL);
			/* tohle by se nemelo stavat .. */
			if (newcli >= FD_SETSIZE) {
				close(newcli);
				continue;
			}
			if (newcli >= fdmax) fdmax = newcli+1;
			fcntl(newcli, F_SETFL, O_RDWR|O_NONBLOCK);
			FD_SET(newcli, &cliset);
		}

		for (i = 0; i < fdmax; i++) if (FD_ISSET(i, &wset)) {
			int wrote = write(i, snapbuf + clibufpos[i], snaplen-clibufpos[i]);
			FD_CLR(i, &wset);
			/* nejakej error, diskonekt */
			if (wrote < 0) {
				FD_CLR(i, &cliset);
				close(i);
				continue;
			}
			/* klient jeste nedostal celej buffer, voziv ho */
			if ((clibufpos[i] += wrote) < snaplen)
				FD_SET(i, &wset);
		}
		/* jeste nenastal cas .. */
		if (tv.tv_sec | tv.tv_usec) continue;
		tv.tv_sec = TICK;

		/* zabij sechny klienty co nemaj dopsano .. */
		for (i = 0; i < fdmax; i++) if (FD_ISSET(i, &cliset)) {
			if (clibufpos[i] < snaplen) {
				FD_CLR(i, &cliset);
				close(i);
			}
			clibufpos[i] = 0;
		}
		ts = (uint32_t) time(NULL);
#ifdef SQLITE
		sqlite3_exec(dbh, "BEGIN;", NULL, NULL, NULL);
#endif
#ifndef PROCIPTACCOUNT
		snapbuf = getsnap(&ctx, argv[1], snapbuf, &snaplen);
#else
		snapbuf = getsnap(fn, snapbuf, &snaplen);
#endif
#ifdef SQLITE
		sqlite3_exec(dbh, "COMMIT;", NULL, NULL, NULL);
#endif
	}
}
